//
//  ViewController.swift
//  Mico Pets
//
//  Created by Gleb on 25.04.2020.
//  Copyright © 2020 Gleb and Co. All rights reserved.
//

import UIKit

class SearchViewController: UITableViewController {
    
    lazy var searchController: UISearchController = {
        let s = UISearchController(searchResultsController: nil)
        s.searchResultsUpdater = self
        s.obscuresBackgroundDuringPresentation = false
        s.searchBar.placeholder = "Search your future pet"
        s.searchBar.sizeToFit()
        s.searchBar.searchBarStyle = .prominent
        s.searchBar.scopeButtonTitles = ["Young","Small","Medium"]
        s.searchBar.delegate = self
        return s
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.searchController = searchController
        // Do any additional setup after loading the view.
        
        if #available(iOS 13, *) {
            // Attaching the search controller at this time on iOS 13 results in the
            // search bar being initially visible, so assign it later
        }
        else {
            navigationItem.searchController = searchController
        }
    }
    
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
    
    
    return cell
    }
    
   
}

extension SearchViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        
    }
}

extension SearchViewController: UISearchResultsUpdating{
    func updateSearchResults(for searchController: UISearchController) {
        
    }
}
